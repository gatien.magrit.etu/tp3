import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);

		const form = document.querySelector('.pizzaForm'), // balise form
			input = form.querySelector('input[name=name]'); // balise <input name="message">

		form.addEventListener('submit', event => {
			// détection de la soumission
			event.preventDefault(); // on empêche la page de se recharger
			if (input.value) {
				alert(`La pizza ${input.value} a été ajoutée !`);
				input.value = '';
			} else {
				alert('Nom du formulaire vide !');
			}
		});
	}

	submit(event) {}
}

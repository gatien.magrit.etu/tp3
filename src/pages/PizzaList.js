import PizzaThumbnail from '../components/PizzaThumbnail.js';
import Page from './Page';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList'); // on pase juste la classe CSS souhaitée
		this.pizzas = pizzas;
	}

	/**
	 * setter de la liste des pizzas.
	 * Génère autant de composants `PizzaThumbnail` que de pizzas dans le tableau
	 * et les stocke dans `this.children`
	 * @param {Array} value Tableau d'objets pizza à afficher
	 */
	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}

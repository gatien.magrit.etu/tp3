import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';
import { doc } from 'prettier';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas

const logo = document.querySelector('.logo');
logo.innerHTML = `${logo.innerHTML}<small>les pizzas c'est la vie</small>`;

const menu = document.querySelector('.pizzaListLink');
const attr = menu.getAttribute('class');
menu.setAttribute('class', `${attr} active`);

const newsContainer = document.querySelector('.newsContainer');
newsContainer.setAttribute('style', "display:''");

const close = document.querySelector('.closeButton');
close.addEventListener('click', event => {
	newsContainer.setAttribute('style', 'display:none');
});
